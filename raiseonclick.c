/* Copyright (C) 2017 by Roland Haas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cache.h"

Display *dpy;
Window root;

int main(int argc, char **argv)
{
  XEvent ev;

  Window oldWin, newWin, topWin;
  Time oldTime, newTime;
  int event_mode;

  int format;
  Atom real;
  unsigned long n, extra;
  unsigned char *data;

  Atom NET_ACTIVE_WINDOW;

  if (!(dpy = XOpenDisplay(NULL)))
  {
    fprintf(stderr, "raisonclick: can't open display\n");
    exit(1);
  }

  root = DefaultRootWindow(dpy);
  NET_ACTIVE_WINDOW = XInternAtom(dpy, "_NET_ACTIVE_WINDOW",0);

  XGrabButton(dpy, 1, AnyModifier, root,
              False, ButtonPressMask | ButtonReleaseMask,
              GrabModeSync, GrabModeAsync, None, None);

  oldTime = 0;
  oldWin = root;
  topWin = None;

  for(;;)
  {
    XNextEvent(dpy, &ev);

    if(ev.type == ButtonPress)
    {
      newWin = ev.xbutton.window;
      newTime = ev.xbutton.time;

      event_mode = ReplayPointer;

      if(newWin == oldWin && newTime - oldTime < 500)
      {
        XEvent xev;

        Window rr, cr, cw;
        int rx, ry, cx, cy;
        unsigned int mr;

        XQueryPointer(dpy, root, &rr, &cr, &rx, &ry, &cx, &cy, &mr);
        if (cr)
        {
          cw = client_window(cr);

          if (cw != topWin) {
            xev.type = ClientMessage;
            xev.xclient.display = dpy;
            xev.xclient.window = cw;
            xev.xclient.message_type = NET_ACTIVE_WINDOW;
            xev.xclient.format = 32;
            xev.xclient.data.l[0] = 2L;
            xev.xclient.data.l[1] = CurrentTime;

            XSendEvent(dpy,root,False, SubstructureNotifyMask | SubstructureRedirectMask, &xev);
            event_mode = AsyncPointer;
          }
        }
      }

      if(XGetWindowProperty(dpy, root, NET_ACTIVE_WINDOW, 0, ~0, False,
                            AnyPropertyType, &real, &format, &n, &extra,
                            &data) == Success && data != 0 && format == 32)
      {
        topWin = (Window)*(unsigned long *) data;
        XFree(data);
      }

      XAllowEvents(dpy, event_mode, ev.xbutton.time);

      oldWin = newWin;
      oldTime = newTime;
    }
  }

  return 0;
}
