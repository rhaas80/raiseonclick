/* cache.h: prototypes for the cache functions */

/* Copyright (C) 1999 by the Massachusetts Institute of Technology.
 *
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting
 * documentation, and that the name of M.I.T. not be used in
 * advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 */

typedef struct _ci {
  struct _ci *next;
  Window win, parent;
  int x, y, w, h, mapped;
  int numnames;
  char *names[1];
} cacheinfo;

#define NAWM_CACHE_CLIENT (1 << 0)
#define NAWM_CACHE_PARENT (1 << 1)
#define NAWM_CACHE_WINDOW (NAWM_CACHE_CLIENT | NAWM_CACHE_PARENT)
#define NAWM_CACHE_MAPPED (1 << 2)
#define NAWM_CACHE_LOCATION (1 << 3)
#define NAWM_CACHE_NAME (1 << 4)
#define NAWM_CACHE_RETRY (1 << 5)

void initcache(void);
void update_cache(XEvent *ev);
Window client_window(Window win);
Window manager_window(Window win);
int hasname(Window win, char *name);
int find_window(cacheinfo *match, long mask);

void dumpcache(int signum);

