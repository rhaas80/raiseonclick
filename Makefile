.PHONY: clean

raiseonclick: raiseonclick.c cache.c cache.h
	cc raiseonclick.c cache.c -lX11 -o raiseonclick

clean:
	rm -f raiseonclick
