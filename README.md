raiseonclick
============

A simple tool that raises a window if you double click anywhere in it. 

To compile
```
cc raiseonclick.c cache.c -lX11 -o raiseonclick
```
