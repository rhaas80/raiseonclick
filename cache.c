/* cache.c: the window cache */

/* Copyright (C) 1999 by the Massachusetts Institute of Technology.
 *
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting
 * documentation, and that the name of M.I.T. not be used in
 * advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include "cache.h"

extern Display *dpy;
extern Window root;

static Atom XA_WM_STATE = None;

int is_client_window(Window win);
Window client_window(Window win);
Window client_window_internal(Window win);

Window client_window(Window win)
{
  Window cw;

  if (XA_WM_STATE == None)
    XA_WM_STATE = XInternAtom(dpy, "WM_STATE", False);

  /* actually look for it. */
  cw = client_window_internal(win);
  if (cw)
    return cw;

  return win;
}

Window client_window_internal(Window win)
{
  Window root, parent;
  Window *children;
  unsigned int nchildren;
  unsigned int i;
  Window inf = 0;

  if (is_client_window(win))
    return win;

  if (!XQueryTree(dpy, win, &root, &parent, &children, &nchildren))
    return 0;
  for (i = 0; !inf && (i < nchildren); i++)
    inf = client_window_internal(children[i]);

  if (children)
    XFree((char *)children);
  return inf ? inf : 0;
}

int is_client_window(Window win)
{
  Atom type = None;
  int format;
  unsigned long nitems, after;
  unsigned char *data = NULL;

  XGetWindowProperty(dpy, win, XA_WM_STATE, 0, 0, False, AnyPropertyType,
		     &type, &format, &nitems, &after, &data);
  if (data)
    XFree((char*)data);
  if (type)
    return 1;
  return 0;
}

